---
title: 'আনুষ্ঠানিক ক্লাব প্রতিষ্ঠা'
excerpt: '‘কোনও একটি বিষয়ে মানুষকে বিশেষজ্ঞ বানানোই যথেষ্ট নয়। সুপ্রশিক্ষিত করে তাকে ভালো যন্ত্র বানানো যেতে পারে, কিন্তু তার মনুষ্যত্বের বিকাশ ঘটানো সম্ভব নয়। একজন শিক্ষার্থীর সুন্দর-অসুন্দরের চেতনা, ভালো বিচার করার ক্ষমতা এবং মূল্যবোধের প্রতি আকর্ষণ থাকা অত্যন্ত প্রয়োজন। না হলে কেবল কিছু বিষয়ে বিশেষত্ব অর্জন করলে তাকে প্রশিক্ষিত কুকুর বলা যায়, উন্নত মানুষ নয়।’.'
coverImage: '/assets/blog/dynamic-routing/cover.jpg'
date: '2020-04-16T05:35:07.322Z'
author:
  name: JJ Kasper
  picture: '/assets/blog/authors/jj.jpeg'
ogImage:
  url: '/assets/blog/dynamic-routing/cover.jpg'
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Praesent elementum facilisis leo vel fringilla est ullamcorper eget. At imperdiet dui accumsan sit amet nulla facilities morbi tempus. Praesent elementum facilisis leo vel fringilla. Congue mauris rhoncus aenean vel. Egestas sed tempus urna et pharetra pharetra massa massa ultricies.