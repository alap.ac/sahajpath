import Container from './container'
import cn from 'classnames'
import { EXAMPLE_PATH } from '../lib/constants'

export default function Alert({ preview }) {
  return (
    <div
      className={cn('border-b', {
        'bg-blue-200 border-accent-7 text-white': preview,
        'bg-blue-200 border-accent-2': !preview,
      })}
    >
      <Container>
        <div className="py-2 text-center text-sm">
          {preview ? (
            <>
              This page is a preview.{' '}
              <a
                href="/api/exit-preview"
                className="underline hover:text-cyan duration-200 transition-colors"
              >
                Click here
              </a>{' '}
              to exit preview mode.
            </>
          ) : (
            <>
              ক্লাবের ওয়েবসাইট তৈরির কাজ চলছে{' '}
              <a
                href={`https://github.com/vercel/next.js/tree/canary/examples/${EXAMPLE_PATH}`}
                className="text-blue-700 hover:text-success duration-200 transition-colors"
              >
                আরও জানুন
              </a>
              .
            </>
          )}
        </div>
      </Container>
    </div>
  )
}
