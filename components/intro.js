import { CMS_NAME } from '../lib/constants'

export default function Intro() {
  return (
    <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
      <h1 className="text-6xl md:text-8xl font-bold tracking-tighter text-red-700 leading-tight md:pr-8">
        আলাপ
      </h1>
      <h2 className="text-center text-red-700 md:text-left text-lg md:pl-8">
        বিজ্ঞান পরিচয়
      </h2>
    </section>
  )
}
