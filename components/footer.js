import Container from './container'
import { EXAMPLE_PATH } from '../lib/constants'

export default function Footer() {
  return (
    <footer className="bg-accent-1 mt-5 rounded-lg border-t border-accent-2">
      <Container>
        <div class="text-center py-4 text-xl text-gray-800">
<h1>যোগাযোগ</h1>
<p>Email: alap.sc@criptext.com</p>
</div>
      </Container>
    </footer>
  )
}
