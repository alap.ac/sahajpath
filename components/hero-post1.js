import Avatar from '../components/avatar'
import DateFormater from '../components/date-formater'
import CoverImage from '../components/cover-image'
import Link from 'next/link'

export default function HeroPost1({
  title,
  coverImage,
  date,
  excerpt,
  author,
  slug,
}) {
  return (
    <section>
        <div>
          <p className="text-lg text-justify leading-relaxed mb-4">{excerpt}</p>
      </div>
    </section>
  )
}




