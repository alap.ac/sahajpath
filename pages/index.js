import Container from '../components/container'
import MoreStories from '../components/more-stories'
import HeroPost from '../components/hero-post'
import HeroPost1 from '../components/hero-post1'
import HeroPost2 from '../components/hero-post2'

import Intro from '../components/intro'
import Layout from '../components/layout'
import { getAllPosts } from '../lib/api'
import Head from 'next/head'
import { CMS_NAME } from '../lib/constants'

export default function Index({ allPosts }) {
  const heroPost = allPosts[0]
  const heroPost1 = allPosts[1]
  const heroPost2 = allPosts[2]
  const morePosts = allPosts.slice(1)
  return (
    <>
      <Layout>
        <Head>
          <title>Next.js Blog Example with {CMS_NAME}</title>
        </Head>
        <Container>
          <Intro />
  

  
<div className="container rounded-lg border-2 border-green-800">

 <div class="text-center mb-3 text-xl text-green-800">
<h1>সাম্প্রতিক পোস্ট</h1>
</div>

<section class="text-gray-700 body-font">
  <div class="container mx-auto">
    <div class="flex flex-wrap -m-4">
      <div class="p-6 md:w-1/3">
        <div class="flex rounded-lg h-full bg-green-100 p-4 flex-col">
          <div class="flex items-center">

     {heroPost && (
            <HeroPost
              title={heroPost.title}
              coverImage={heroPost.coverImage}
              date={heroPost.date}
              author={heroPost.author}
              slug={heroPost.slug}
              excerpt={heroPost.excerpt}
            />
          )}

</div>
</div>
</div>
</div>

    <div class="flex flex-wrap -m-4">
      <div class="p-6 md:w-1/3">
        <div class="flex rounded-lg h-full bg-green-100 p-4 flex-col">
          <div class="flex items-center">

  {heroPost1 && (
            <HeroPost1
              title={heroPost1.title}
              coverImage={heroPost1.coverImage}
              date={heroPost1.date}
              author={heroPost1.author}
              slug={heroPost1.slug}
              excerpt={heroPost1.excerpt}
            />
          )}


</div>
</div>
</div>
</div>

    <div class="flex flex-wrap -m-4">
      <div class="p-6 md:w-1/3">
        <div class="flex rounded-lg h-full bg-green-100 p-4 flex-col">
          <div class="flex items-center">
  {heroPost2 && (
            <HeroPost2
              title={heroPost2.title}
              coverImage={heroPost2.coverImage}
              date={heroPost2.date}
              author={heroPost2.author}
              slug={heroPost2.slug}
              excerpt={heroPost2.excerpt}
            />
          )}

</div>
</div>
</div>
</div>
 </div>
</section>
 </div>
        </Container>
      </Layout>
    </>
  )
}

export async function getStaticProps() {
  const allPosts = getAllPosts([
    'title',
    'date',
    'slug',
    'author',
    'coverImage',
    'excerpt',
  ])

  return {
    props: { allPosts },
  }
}
